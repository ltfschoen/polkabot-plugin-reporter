import BN from 'bn.js'
import pluginConfig from './config'

// TODO: Will move to hash later on
import xxhash from '@polkadot/util-crypto/xxhash/xxhash64/asHex'

module.exports = class Reporter {
  constructor (pbot) {
    this.pbot = pbot
    this.config = pluginConfig
    this.cache = {}
  }

  start () {
    console.log('Reporter - Starting with config:', this.config)
    this.watchChain().catch((error) => {
      console.error('Reporter - Error subscribing to chain: ', error);
    });
  }

  announce (msg) {
    console.log('Reporter - Announcing: ', msg)
    this.pbot.matrix.sendTextMessage(this.pbot.config.matrix.roomId, msg)
  }

  buf2hex (buffer) { // buffer is an ArrayBuffer
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('')
  }

  async subscribeChainBlockHeader () {
    // Keep track of the block header
    //
    // Reference: https://polkadot.js.org/api/examples/promise/02_listen_to_blocks/
    await this.pbot.polkadot.rpc.chain
      .subscribeNewHead((header) => {
        const KEY = 'blockNumber'

        if (header) {
          this.cache[KEY] = new BN(header.blockNumber, 16)
        }
      })
  }

  async minimumBlockPeriodChanged () {
    // Keep track of the block target time in seconds
    //
    // Note: `minimumPeriod` replaces `blockPeriod`
    const minimumPeriod = await this.pbot.polkadot.query.timestamp.minimumPeriod();
    const KEY = 'minimumPeriod'
    const minimumPeriodNumber = new BN(Number(minimumPeriod))

    if (!this.cache[KEY]) this.cache[KEY] = minimumPeriodNumber
    if (this.cache[KEY] && !(this.cache[KEY].eq(minimumPeriodNumber))) {
      this.cache[KEY] = minimumPeriodNumber
      console.log(`Reporter - minimumPeriod changed to ${minimumPeriodNumber} s`)
      this.announce(`The minimum block period has changed. It is now ${minimumPeriodNumber.toNumber().toFixed(2)} seconds`)
    }
  }

  async minimumDepositForProposalsChanged () {
    const minimumDeposit = await this.pbot.polkadot.query.democracy.minimumDeposit();
    const KEY = 'minimumDeposit'
    const minimumDepositNumber = Number(minimumDeposit)
    console.log('Reporter - Minimum deposit:', minimumDepositNumber)

    if (!this.cache[KEY]) this.cache[KEY] = minimumDepositNumber
    if (this.cache[KEY] && this.cache[KEY] !== (minimumDepositNumber)) {
      this.cache[KEY] = minimumDepositNumber
      console.log(`Reporter - minimumDeposit changed to ${minimumDepositNumber} s`)
      this.announce(`The minimum deposit for proposals has changed. It is now ${minimumDepositNumber.toFixed(2)} µDOTs`)
    }
  }

  async activeValidatorCountChanged () {
    const validators = await this.pbot.polkadot.query.session.validators();
    const KEY = 'validator'

    if (!this.cache[KEY]) this.cache[KEY] = validators
    if (this.cache[KEY] && this.cache[KEY].length !== validators.length) {
      this.cache[KEY] = validators
      console.log('Reporter - Active Validator count: ', validators.length)
      console.log('Reporter - Validators', validators)
      this.announce(`The active Validator count has changed. It is now ${validators.length}`)
    }
  }

  async validatorSlotCountChanged () {
    const validatorCount = await this.pbot.polkadot.query.staking.validatorCount();
    const KEY = 'validatorCount'

    if (!this.cache[KEY]) this.cache[KEY] = validatorCount
    if (this.cache[KEY] && this.cache[KEY] !== validatorCount) {
      this.cache[KEY] = validatorCount
      console.log('Reporter - Validator count:', validatorCount.toString(10))
      this.announce(`The number of validator slots has changed. It is now ${validatorCount.toString(10)}`)
    }
  }

  async runtimeCodeChanged () {
    const code = await this.pbot.polkadot.query.substrate.code();
    const KEY = 'runtime'
    const hash = xxhash(code)

    if (!this.cache[KEY]) this.cache[KEY] = { hash, code }
    if (this.cache[KEY] && this.cache[KEY].hash !== hash) {
      this.cache[KEY] = { hash, code }
      console.log('Reporter - Runtime Code hash changed:', hash)

      // const codeInHex = '0x' + this.buf2hex(code)
      // console.log('Runtime Code hex changed', codeInHex)

      this.announce(`Runtime code hash has changed. The hash is now ${hash}. The runtime is now ${code ? (code.length / 1024).toFixed(2) : '???'} kb.`)
    }
  }

  async councilVotingPeriod () {
    // we don't alert for changes but store the latest value
    const votingPeriod = await this.pbot.polkadot.query.council.votingPeriod();
    const KEY = 'votingPeriod'

    this.cache[KEY] = new BN(votingPeriod)
    console.log('Reporter - VotingPeriod:', this.cache[KEY].toString(10))
  }

  async councilMotionsProposalCountChanged () {
    const proposalCount = await this.pbot.polkadot.query.councilMotions.proposalCount();
    const KEY = 'proposalCount'

    const count = new BN(proposalCount)
    if (!this.cache[KEY]) this.cache[KEY] = count
    if (this.cache[KEY] && !(this.cache[KEY].eq(count))) {
      this.cache[KEY] = count

      console.log('Reporter - Proposal count changed:', count.toString(10))
      const id = count.sub(new BN(1))
      this.announce(
        `A new proposal is available (#${id}), check your UI at https://polkadot.js.org/apps/#/democracy.
You will be able to vote shortly, a new referendum will show up in the UI.`)
    }
  }

  async referendumCountChanged () {
    const referendumCount = await this.pbot.polkadot.query.democracy.referendumCount();
    const KEY = 'referendumCount'
    console.log('Reporter - referendumCount:', referendumCount)

    const count = new BN(referendumCount)
    if (!this.cache[KEY]) this.cache[KEY] = count
    if (this.cache[KEY] && !(this.cache[KEY].eq(count))) {
      this.cache[KEY] = count
      const deadline = this.cache.blockNumber.add(this.cache.votingPeriod)
      const votingTimeInMinutes = parseInt(this.cache.votingPeriod.mul(this.cache.minimumPeriod).toString(10)) / 60
      console.log('Reporter - Referendum count changed:', count.toString(10))
      const id = count.sub(new BN(1)).toString(10)

      this.announce(
        `@room New referendum (#${id}) available. Check your UI at https://polkadot.js.org/apps/#/democracy.
You can vote for referendum #${id} during the next ${this.cache.votingPeriod.toString(10)} blocks. 
That means a deadline at block #${deadline.toString(10)}, don't miss it! 
You have around ${votingTimeInMinutes.toFixed(2)} minutes to vote.`)
    }
  }

  async watchChain () {
    await this.subscribeChainBlockHeader();
    await this.minimumBlockPeriodChanged();
    await this.minimumDepositForProposalsChanged();
    await this.activeValidatorCountChanged();
    await this.validatorSlotCountChanged();
    await this.runtimeCodeChanged();
    await this.councilVotingPeriod();
    await this.councilMotionsProposalCountChanged();
    await this.referendumCountChanged();
  }
}
